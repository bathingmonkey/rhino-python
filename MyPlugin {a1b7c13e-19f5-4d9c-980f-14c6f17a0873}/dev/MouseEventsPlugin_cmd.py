import Rhino 
from Rhino.UI import *
import rhinoscript.userinterface
import rhinoscript.geometry
import scriptcontext


__commandname__ = "MouseEventsPlugin"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"


class MyMouseCallback(MouseCallback):
    def OnMouseDown(self, e):
        print "Mouse-UP"
    def OnMouseUp(self, e):
        print "Mouse-Down"



def RunCommand( is_interactive ):
    '''
    '''
    if scriptcontext.sticky.has_key("MouseEventsPlugin"):
        m_mc = scriptcontext.sticky["MouseEventsPlugin"]
    else:
        m_mc = MyMouseCallback()
        scriptcontext.sticky["MouseEventsPlugin"] = m_mc
        
    m_mc.Enabled = not m_mc.Enabled
    if m_mc.Enabled:
        print "Start", __commandname__
    else:
        print "End", __commandname__
        
    return Rhino.Commands.Result.Success

#if( __name__ == "__main__" ):
#    RunCommand()
