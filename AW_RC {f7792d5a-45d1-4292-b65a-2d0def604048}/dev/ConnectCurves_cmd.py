import Rhino.Input.Custom as ric
import Rhino.Input as ri
import Rhino.Commands as rhc
import Rhino.Geometry as rhg
import Rhino.DocObjects as rhd
import Rhino.Display as rd
import rhinoscriptsyntax as rs
import scriptcontext as sc
import string
from collections import OrderedDict
import System
import functools 

__commandname__ = "ConnectCurves"


# Constants
tol = sc.doc.ModelAbsoluteTolerance
a_tol = sc.doc.ModelAngleToleranceDegrees


class HighlightCurvesConduit(rd.DisplayConduit):     
    def __init__(self, crv_ids):
        self.curves = crv_ids
    def DrawForeground(self, e):
        w = 4
        c = System.Drawing.Color.FromArgb(255, 0, 255, 0) #.Green
        if e.Viewport.Name == "Top":
            for id in self.curves:
                crv = e.RhinoDoc.Objects.Find(id).CurveGeometry
                if crv:
                    e.Display.DrawCurve(crv, c, w)


def PostProcess(f):
    """Close ConnectCurves()"""
    
    @functools.wraps(f)
    def wrapper(arg):
        isCopy = rs.GetBoolean("Result", [["Copy", "no", "yes"]], [True])
        ids, result, ids_base_crv = f(arg)
        if result in [rhc.Result.Success, ri.GetResult.Cancel]:
            if len(ids) < 2:
                rs.ShowObjects(ids_base_crv)
                return ids, result
            rs.EnableRedraw(False)
            [rs.ShowObject(i) for i in ids_base_crv if i != None]
            if not isCopy[0]:
                [rs.DeleteObjects(i) for i in ids_base_crv if i != None]
            joined_crv = rs.JoinCurves(ids, True)
            rs.EnableRedraw(True)
            rs.Redraw()
        elif result in [rhc.Result.Failure,]:
            rs.DeleteObjects(ids)
            [rs.ShowObject(i) for i in ids_base_crv if i != None]
            joined_crv = None
            
        return joined_crv, result
        
    return wrapper


@PostProcess
def RunCommand(is_interactive):
    """Connect curves by selecting them one by one"""
    print __commandname__
    #
    ids = []  # resulted curve
    ids_base_crv = []  # original curves
    selected_pt = []
    radius = 0.0

    #
    go = ric.GetObject()
    go.EnablePreSelect(False, ignoreUnacceptablePreselectedObjects=True)
    go.GeometryFilter = rhd.ObjectType.Curve
    go.GeometryAttributeFilter = ric.GeometryAttributeFilter.OpenCurve
    
    #
    conduit = HighlightCurvesConduit(ids)
    conduit.Enabled = True
    
    #
    while True:
        go.AcceptUndo(len(ids)!=0)
        if radius == 0:
            if ids:
                go.ClearDefault() 
                go.AcceptNumber(True, True)
                go.SetCommandPrompt("Select a curve <input radius>")
                go.AddOption("GetArc")
            else:
                go.AcceptNumber(False, True)
                go.ClearCommandOptions()
                go.SetCommandPrompt("Select a curve")
               
        else:
            go.ClearDefault()
            go.AcceptNumber(True, True)
            go.SetCommandPrompt("Select a curve") 
            go.SetCommandPromptDefault("radius=" + str(radius))
        res = go.Get()  
        
        #
        if res == ri.GetResult.Object:
            selected_pt1 = go.Object(0).SelectionPoint()
            crv1_id = go.Object(0).ObjectId
            if crv1_id in ids_base_crv: continue 
            if crv1_id in ids: ids.remove(crv1_id)
            crv1 = go.Object(0).Curve()
            
            ## Create Fillet
            if len(ids) == 0:
                new_crvs = [rs.CopyObject(crv1_id)]
                base_crvs = [crv1_id]
                pt = [selected_pt1]
            else:
                selected_pt0 = selected_pt[-1]
                crv0_id = ids[-1]#ids.pop()
                crv0 = rhd.ObjRef(crv0_id).Curve()
                curves = rhg.Curve.CreateFilletCurves(crv0, selected_pt0, crv1,
                    selected_pt1, radius, False, True, True, tol, a_tol)

                if len(curves) == 2:
                    rs.DeleteObject(ids.pop())
                    id0 = sc.doc.Objects.AddCurve(curves[0])
                    id1 = sc.doc.Objects.AddCurve(curves[1])
                    new_crvs = [id0, id1]
                    base_crvs = [crv1_id]
                    pt = [selected_pt1]

                elif len(curves) == 3:
                    rs.DeleteObject(ids.pop())
                    id0 = sc.doc.Objects.AddCurve(curves[0])
                    id1 = sc.doc.Objects.AddCurve(curves[1])
                    id2 = sc.doc.Objects.AddCurve(curves[2]) # fillet
                    new_crvs = [id0, id2, id1]
                    base_crvs = [None, crv1_id]
                    pt = [None, selected_pt1]

                else:
                    rs.MessageBox("Error: Can't join the curve.") # if lines are parallel.
                    continue
                radius = 0.0
            
            ##
            ids += new_crvs
            ids_base_crv += base_crvs 
            selected_pt += pt 
            [rs.HideObject(i) for i in ids_base_crv if i != None]
            #go.AcceptUndo(True)
            
            ##
            if len(ids) < 2: continue
            curves = [rs.coercecurve(id, -1, True) for id in ids]
            joined_crv = rhg.Curve.JoinCurves(curves, tol)
            if len(joined_crv) == 1:
                if joined_crv[0].IsClosed:
                    obj_delete = ids_base_crv.pop()
                    rs.ShowObject(obj_delete)   # Delete the last duplicate
                    rs.DeleteObject(obj_delete)   # Delete the last duplicate
                    res = rhc.Result.Success
                    break
                else:
                    continue
            else:
                print "Error: Can't join curves"
                res = rhc.Result.Failure 
                break
        #
        elif res == ri.GetResult.Option:
            while True:
                arc = rs.GetObject("Pick an arc",
                rs.filter.curve, select=True)
                if arc == None: break
                if rs.IsArc(arc):
                    radius = rs.ArcRadius(arc)
                    break
                else:
                    rs.MessageBox("Error: Not Arc.")
                    continue
        #
        elif res == ri.GetResult.Number:
            radius = go.Number()
        #
        elif res == ri.GetResult.Undo:
            if len(ids) == 1:
                id = ids.pop()
                rs.DeleteObject(id)
                rs.ShowObject(ids_base_crv.pop())
            else:
                id = ids.pop()
                rs.DeleteObject(id)
                rs.ShowObject(ids_base_crv.pop())
                if ids_base_crv[-1] == None:
                    rs.DeleteObject(ids.pop()) # Delete fillet
                    ids_base_crv.pop() # Delete None
            
            selected_pt.pop()
            if len(selected_pt) != 0:
                if selected_pt[-1] == None: selected_pt.pop()
            radius = 0.0
        #
        elif res == ri.GetResult.Cancel:
            print "Cancelled"
            break
    # WhileEnd
    
    conduit.Enabled = False
    sc.doc.Views.Redraw()
    
    return ids, res, ids_base_crv



if __name__ == "__main__":
    crv_id, result = RunCommand(True)  
    print crv_id
    print result