import Rhino
import scriptcontext

def MyEvent(sender, e):
    print "Object(s) selected ={0}", e.Selected
    
class MyMouseCallback(Rhino.UI.MouseCallback):
    def OnMouseDown(self, e):
        print "active viewport: {0}".format(e.View.ActiveViewport.Name)   
    
def DoSomething():
    select_event_name = "MyMouseCallback"
    
    if scriptcontext.sticky.has_key(select_event_name):#"has_key" is python
        print "removing callback"
        func = scriptcontext.sticky[select_event_name]
        Rhino.RhinoDoc.SelectObjects -= func
        #Rhino.RhinoDoc.DeselectObjects -= func
        scriptcontext.sticky.Remove(select_event_name)
    else:
        print "adding callback"
        func = eval(select_event_name)
        scriptcontext.sticky[select_event_name] = func
        Rhino.RhinoDoc.SelectObjects += func
        #Rhino.RhinoDoc.DeselectObjects += func

DoSomething()

#There are 2 "deselect" events you can watch:
#
#RhinoDoc.DeselectObjects
#RhinoDoc.DeselectAllObjects
#
#If you have a bunch of selected objects and you hold the key and click on one\
# of the selected objects, a RhinoDoc.DeselectObjects event will be triggered.
#
#If you click in the viewport and not on any object,
# the RhinoDoc.DeselectAllObjects event will be triggered.