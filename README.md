This is my Python scripts of custom Rhino commands.  

The scripts run in Rhino 6. (not confirmed in Rhino 5 and Mac)

Installation: Extract the zipped folder and place it in %APPDATA%\McNeel\Rhinoceros\6.0\Plug-ins\PythonPlugIns\ on your own computers. Restart Rhino.


## [1.0.5] - 2020-09-15

## TogglePanels ***Added***
[GitLab](dev/TogglePanels_cmd.py)

## MoveSketchLayer   
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/MoveSketchLayer_cmd.py)

## SlotCenter 
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/SlotCenter_cmd.py)  

## SlotCenterDist
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/SlotCenterDistance_cmd.py)  
![alt text](/images/SlotCenterDist.gif)

## SlotCorner
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/SlotCorner_cmd.py)  
![alt text](/images/SlotCorner.gif)

## ChangeObjectColor
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/ChangeObjectColor_cmd.py)

## GridSwitch
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/GridSwitch_cmd.py)

## HideLockedObjects
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/HideLockedObjects_cmd.py)

## ShowLockedObjects
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/ShowLockedObjects_cmd.py)

## toggle_gumball_drag_mode
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/toggle_gumball_drag_mode_cmd.py)