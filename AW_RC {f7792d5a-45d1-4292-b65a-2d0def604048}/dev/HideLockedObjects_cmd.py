# 2017/7/26
import rhinoscriptsyntax as rs
import scriptcontext
from Rhino import *
from scriptcontext import doc


__commandname__ = "HideLockedObjects"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def RunCommand( is_interactive ):
    #print __commandname__
    
    ids = rs.LockedObjects(include_lights=False, include_grips=False)
    if not ids:
        print "No locked objects"
        return Commands.Result.Failure

    rs.UnlockObjects(ids)
    rs.HideObjects(ids)


    m_ids = []
    if scriptcontext.sticky.has_key("LockedObjects"):
        m_ids = scriptcontext.sticky["LockedObjects"]
        for id in ids:m_ids.append(id)
    else:
        m_ids = ids

    scriptcontext.sticky["LockedObjects"] = m_ids
    print scriptcontext.sticky["LockedObjects"]

    # you can optionally return a value from this function
    # to signify command result. Return values that make
    # sense are
    #   0 == success
    #   1 == cancel
    # If this function does not return a value, success is assumed
    return Commands.Result.Success
  
def LockedObjects(include_lights=False, include_grips=False):
    # all non-light objects that are selected
    object_enumerator_settings = DocObjects.ObjectEnumeratorSettings()
    object_enumerator_settings.IncludeLights = include_lights
    object_enumerator_settings.IncludeGrips = include_lights
    object_enumerator_settings.NormalObjects = False
    object_enumerator_settings.LockedObjects = True
    object_enumerator_settings.HiddenObjects = False
    object_enumerator_settings.ReferenceObjects = False
    object_enumerator_settings.SelectedObjectsFilter = False
    selected_objects = doc.Objects.GetObjectList(object_enumerator_settings)
    ids = [rhobj.Id for rhobj in selected_objects]
    return ids
  

#This allows you to test the script from the editor, debug etc.
#RunCommand(True)

if( __name__ == "__main__" ):
    RunCommand(True)
