import rhinoscriptsyntax as rs
import Rhino
import scriptcontext

__commandname__ = "DisplayIsocurveToggle"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def RunCommand( is_interactive ):
    print __commandname__

    view = scriptcontext.doc.Views.ActiveView
    displaymode = view.ActiveViewport.DisplayMode
    displayattr = displaymode.DisplayAttributes
    
    displayattr.ShowIsoCurves = not displayattr.ShowIsoCurves
    
    Rhino.Display.DisplayModeDescription.UpdateDisplayMode(displaymode)
    rs.Redraw()

    return 0

if __name__ == "__main__":
    RunCommand(True)