# 2010/06/07

import rhinoscriptsyntax as rs
#from System.Drawing import Color

__commandname__ = "MoveSketchLayer"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filename minus "_cmd.py"


def RunCommand(is_interactive):
    '''
    Add a layer "SKETCH" to current-working layer and move points and lines to it
    '''
    print __commandname__

    layer = "SKETCH"

    #
    geometry_type = rs.filter.curve + rs.filter.point

    ids = rs.ObjectsByType(geometry_type, False)
    if not ids:
        print "No object to move"
        return

    ids = list(extract_obj_iter(ids))
    # ids = [id for id in ids if (not rs.IsObjectHidden(id) and
    #                            not rs.IsObjectLocked(id) and
    #                            rs.ObjectLayer(id)== rs.CurrentLayer())]

    if not ids:
        print "Objects are hidden, locked or no object in current layer"
        return

    # SKETCH layer is the current layer?
    if rs.IsLayer(layer):
        if rs.IsLayerCurrent(layer):  # here, if no such layer error happens
            print "Currently in SKETCH layer"
            return

    # make SKETCH layer or already?
    layer_full = rs.CurrentLayer() + "::" + layer
    if not rs.IsLayer(layer_full):
        rs.AddLayer(layer, rs.LayerColor(rs.CurrentLayer()),
                    parent=rs.CurrentLayer())

    # move objects
    [rs.ObjectLayer(id, layer_full) for id in ids]

    n = len(ids)
    print "%s object moved in %s" % (len(ids), layer_full)

    return 0


def extract_obj_iter(ids):
    for _, id in enumerate(ids):
        if (not rs.IsObjectHidden(id) and not rs.IsObjectLocked(id) and rs.ObjectLayer(id) == rs.CurrentLayer()):
            yield id


# This allows you to test the script from the editor, debug etc.
# RunCommand(True)
if(__name__ == "__main__"):
    RunCommand(True)
