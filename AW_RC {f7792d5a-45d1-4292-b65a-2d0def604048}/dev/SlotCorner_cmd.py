import rhinoscriptsyntax as rs
import scriptcontext
import Rhino
import System


__commandname__ = "SlotCorner"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"


def RunCommand(is_interactive):

    print __commandname__

    # Work Plane
    plane = None

    # Draw Object Attributes
    rect_color = Rhino.ApplicationSettings.AppearanceSettings.CrosshairColor
    line_color = scriptcontext.doc.Layers.CurrentLayer.Color
    text_color = System.Drawing.Color.FromArgb(0, 0, 0)

    # Get 1st Corner Point
    onSrf_option = Rhino.Input.Custom.OptionToggle(False, "Off", "On")
    gp = Rhino.Input.Custom.GetPoint()
    gp.SetCommandPrompt("First corner")
    gp.AcceptNumber(enable=True, acceptZero=False)
    gp.AcceptPoint(True)
    gp.AddOptionToggle("OnSrf", onSrf_option)

    while True:

        res = gp.Get()

        if gp.CommandResult() != Rhino.Commands.Result.Success:

            return gp.CommandResult()

        if res == Rhino.Input.GetResult.Option:

            if gp.Option().EnglishName == "OnSrf":

                if onSrf_option.CurrentValue:

                    spsf = SelPlanarSurface()
                    if spsf[0]:
                        srf = spsf[1]
                    else:
                        gp.ClearCommandOptions()
                        onSrf_option.CurrentValue = False
                        gp.AddOptionToggle("OnSrf", onSrf_option)
                        continue

                    tgp = srf.TryGetPlane()

                    plane = tgp[1] if tgp[0] else None

                    #gp.Constrain(plane, allowElevator=False)
                    gp.Constrain(srf, allowPickingPointOffObject=False)
                else:

                    gp.ClearConstraints()

            continue

        elif res == Rhino.Input.GetResult.Point:

            p1 = gp.Point()

        break

    def GetPointDynamicDrawFunc(sender, args):

        # gp = args.Source  # Dynamically obtain Getpoint instance
        global arc0
        global arc1
        p2 = args.CurrentPoint

        # Draw Diagonal Line
        #diagonal_line = Rhino.Geometry.Line(p1, p2)
        # args.Display.DrawDottedLine(diagonal_line, rect_color) #  Line

        # Move CPlane
        # if type(plane) is Rhino.Geometry.Plane:
        #    view = args.Viewport
        #    cplane = view.GetConstructionPlane()
        #    plane.Origin = p1
        #    cplane.Plane = plane
        #    args.Display.DrawConstructionPlane(cplane)

        # Draw Rectangle
        rect = Rhino.Geometry.Rectangle3d(plane, p1, p2)
        cpt0 = rect.Corner(0)
        cpt1 = rect.Corner(1)
        cpt2 = rect.Corner(2)
        cpt3 = rect.Corner(3)
        l0 = Rhino.Geometry.Line(cpt0, cpt1)
        args.Display.DrawDottedLine(l0, rect_color)
        l1 = Rhino.Geometry.Line(cpt1, cpt2)
        args.Display.DrawDottedLine(l1, rect_color)
        l2 = Rhino.Geometry.Line(cpt2, cpt3)
        args.Display.DrawDottedLine(l2, rect_color)
        l3 = Rhino.Geometry.Line(cpt3, cpt0)
        args.Display.DrawDottedLine(l3, rect_color)

        # Vectors
        vec0 = (cpt1 - cpt0)
        vec0.Unitize()
        vec1 = (cpt2 - cpt1)
        vec1.Unitize()
        vec2 = (cpt3 - cpt2)
        vec2.Unitize()
        vec3 = (cpt0 - cpt3)
        vec3.Unitize()

        # Horizontally Long or Vertically Long ?
        if l0.Length > l1.Length:
            radius = l1.Length / 2
            sp = cpt0 + radius * vec0
            mp = cpt0 - radius * vec3
            ep = cpt3 - radius * vec2
            arc0 = Rhino.Geometry.Arc(sp, mp, ep)
            sp = cpt1 - radius * vec0
            mp = cpt1 + radius * vec1
            ep = cpt2 + radius * vec2
            arc1 = Rhino.Geometry.Arc(sp, mp, ep)
            len = l0.Length
            wid = l1.Length
        else:
            radius = l0.Length / 2
            sp = cpt0 - radius * vec3
            mp = cpt0 + radius * vec0
            ep = cpt1 + radius * vec1
            arc0 = Rhino.Geometry.Arc(sp, mp, ep)
            sp = cpt3 + radius * vec3
            mp = cpt3 - radius * vec2
            ep = cpt2 - radius * vec1
            arc1 = Rhino.Geometry.Arc(sp, mp, ep)
            len = l1.Length
            wid = l0.Length

        # Draw Slot
        args.Display.DrawArc(arc0, line_color)
        args.Display.DrawArc(arc1, line_color)
        args.Display.DrawLine(arc0.StartPoint, arc1.StartPoint, line_color)
        args.Display.DrawLine(arc1.EndPoint, arc0.EndPoint, line_color)

        # Draw Text
        screen_point = Rhino.Geometry.Point2d(10, 30)
        msg = "L={0}\nW={1}\nCen.Dist={2}\nr={3}".format(
            len, wid, len - wid, radius)
        args.Display.Draw2dText(msg, text_color, screen_point, False)

    # Get current cplane
    view = scriptcontext.doc.Views.ActiveView
    if not view:
        return Rhino.Commands.Result.Failure
    cplane = view.ActiveViewport.GetConstructionPlane()
    previous_plane = cplane.Plane

    # Plane to Constrain
    if type(plane) is not Rhino.Geometry.Plane:

        # CPlane
        plane = cplane.Plane
        plane.Origin = p1
        gp.Constrain(plane, allowElevator=False)

    else:

        # OnSrf
        plane.Origin = p1
        cplane.Plane = plane
        view.ActiveViewport.SetConstructionPlane(cplane)

    # Get 2nd Corner Point
    gp.ClearCommandOptions()
    gp.SetCommandPrompt("Second corner")
    gp.SetBasePoint(p1, showDistanceInStatusBar=False)
    gp.DynamicDraw += GetPointDynamicDrawFunc
    gp.Get()

    if(gp.CommandResult() == Rhino.Commands.Result.Success):

        drawSlotCorner(arc0, arc1)

    cplane.Plane = previous_plane
    view.ActiveViewport.SetConstructionPlane(cplane)

    # you can optionally return a value from this function
    # to signify command result. Return values that make
    # sense are
    #   0 == success
    #   1 == cancel
    # If this function does not return a value, success is assumed
    return 0


def SelPlanarSurface():
    go = Rhino.Input.Custom.GetObject()
    go.SetCommandPrompt("Surface to place a point on")
    go.GeometryFilter = Rhino.DocObjects.ObjectType.Surface
    go.SubObjectSelect = True  # to pick one face of brep
    go.DeselectAllBeforePostSelect = True
    go.OneByOnePostSelect = True
    go.Get()
    if go.CommandResult() != Rhino.Commands.Result.Success:
        return (False, None)

    objref = go.Object(0)

    obj = objref.Object()
    if not obj:
        return (False, None)

    obj.Select(False)     # Unselect surface

    surface = objref.Surface()
    if surface and surface.IsPlanar():
        return (True, surface)
    else:
        print "Not planar surface"
        return (False, None)


def drawSlotCorner(arc0, arc1):

    id_arc0 = scriptcontext.doc.Objects.AddArc(arc0)
    id_arc1 = scriptcontext.doc.Objects.AddArc(arc1)

    if rs.IsCurve(id_arc0):
        ln0_spt = rs.CurveStartPoint(id_arc0)
        ln1_ept = rs.CurveEndPoint(id_arc0)
    else:
        print "can't make arc"
        return

    if rs.IsCurve(id_arc1):
        ln0_ept = rs.CurveStartPoint(id_arc1)
        ln1_spt = rs.CurveEndPoint(id_arc1)
    else:
        print "can't make arc"
        return

    id_ln0 = rs.AddLine(ln0_spt, ln0_ept)
    id_ln1 = rs.AddLine(ln1_spt, ln1_ept)

    objs = [id_arc0, id_arc1, id_ln0, id_ln1]
    if objs:
        rs.JoinCurves(objs, delete_input=True, tolerance=None)

    # scriptcontext.doc.Views.Redraw()


if(__name__ == "__main__"):
    RunCommand(True)
