import Rhino 
import Rhino.UI
import System.Drawing
import scriptcontext
import rhinoscriptsyntax as rs



class MouseCallback(Rhino.UI.MouseCallback):
    def OnMouseDown(self, e):
        print "active viewport: {0}".format(e.View.ActiveViewport.Name)
    
    

def RunCommand():
    
    if scriptcontext.sticky.has_key("mymousecallback"):#"has_key" is python2
        m_mc = scriptcontext.sticky["mymousecallback"]
    else:
        m_mc = MouseCallback()
        scriptcontext.sticky["mymousecallback"] = m_mc
    
    m_mc.Enabled = not m_mc.Enabled
    
    if m_mc.Enabled:
        Rhino.RhinoApp.WriteLine("Click somewhere in a vieport ...")
    else:
        Rhino.RhinoApp.WriteLine("MouseCallback end")
    scriptcontext.doc.Views.Redraw()
    scriptcontext.doc.
    return Rhino.Commands.Result.Success

if __name__ == "__main__":
  RunCommand()
  
  
