# 2020/6/26
import Rhino
import scriptcontext as sc

__commandname__ = "GridSize"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filename minus "_cmd.py"


def RunCommand(is_interactive):
    """GridSize"""
    print(__commandname__)

    view = sc.doc.Views.ActiveView
    cp = view.ActiveViewport.GetConstructionPlane()

    gn = Rhino.Input.Custom.GetNumber()
    gn.SetCommandPrompt("Grid Size")
    gn.SetDefaultNumber(cp.GridLineCount)
    gn.AcceptNothing(True)
    gn.SetLowerLimit(1.0, False)
    gn.Get()
    if gn.CommandResult() != Rhino.Commands.Result.Success:
        return gn.CommandResult()

    size = gn.Number()
    cp.GridLineCount = size
    view.ActiveViewport.PushConstructionPlane(cp)
    sc.doc.Views.Redraw()

    print("Grid Size: {0}".format(size))

    return 0


# This allows you to test the script from the editor, debug etc.
# RunCommand(True)
if(__name__ == "__main__"):
    RunCommand(True)
