import rhinoscriptsyntax as rs
import Rhino
import System.Drawing

__commandname__ = "BlendSolid"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def RunCommand( is_interactive ):
  print __commandname__
  
  
  
  def GetSrf(num):
      """
      Return a GID of a selected surface of polysurfaces or a single surface,
      which is planar.
      Return None if nothing selected.
      """
      polysrf = rs.GetObjectEx("Select %s Face " % (num), rs.filter.polysurface)
      if polysrf is None: return None
      
      gid = polysrf[0]
      pt = polysrf[3]
    
      if rs.IsPolysurface(gid):
        srfs = rs.ExplodePolysurfaces(gid)
        for srf in srfs:
            if rs.IsPointOnSurface(srf, pt):
                selected_srf = srf
                #print("%s" % (selected_srf))
            else:
                rs.DeleteObject(srf)
      elif rs.IsSurface(gid):
          selected_srf = gid
          print("%s" % (selected_srf))
    
      if rs.IsSurfacePlanar(selected_srf):
          return selected_srf
      else:
          print("Not Planar")
          return None 
  
  surface1_id = GetSrf("First")
  if surface1_id == None: return
  
  surface2_id = GetSrf("Second")
  if surface2_id == None: return
  
  
  
  
  
  edges = rs.DuplicateEdgeCurves(surf, select = True)
  
  
  
  
  
  
  
  
  
  # Select 2 Edges;
  curves = rs.GetEdgeCurves("Select two Edges", 2, 2, False)
  print curves[0][2]
  print curves[1]
  
  
  curves = (curve1, curve2)
  domain1 = rs.CurveDomain(curve1)
  domain2 = rs.CurveDomain(curve2)
  domain = (domain1[0], domain2[0])
  revs = (False, True)
  cont = (2,2)

  railCurve = rs.AddBlendCurve(curves, domain, revs, cont)
  
  
  #curve = rs.AddLine((5,0,0), (10,0,10))
  #rs.ExtrudeSurface(surface, curve)
  
  # you can optionally return a value from this function
  # to signify command result. Return values that make
  # sense are
  #   0 == success
  #   1 == cancel
  # If this function does not return a value, success is assumed
  return 0
  
RunCommand(True)
