"""
Make a set of XY, YZ and ZY Planes
"""
import rhinoscriptsyntax as rs
import Rhino
import scriptcontext
import System.Guid

def WorldPlanes(length=100, height=100):
    
    
    rs.EnableRedraw(False)
    
    normal_xy = [0, 0, 1]
    normal_yz = [1, 0, 0]
    normal_zx = [0, 1, 0]
    
    plane_xy = rs.WorldXYPlane()
    plane_yz = rs.WorldYZPlane()
    plane_zy = rs.WorldZXPlane()
    
    surf_xy = rs.AddPlaneSurface(plane_xy, length, height)
    surf_yz = rs.AddPlaneSurface(plane_yz, length, height)
    surf_zx = rs.AddPlaneSurface(plane_zy, length, height)
    
    joint_surfs = (surf_xy, surf_yz, surf_zx) 
    
    coordinates = rs.JoinSurfaces(joint_surfs, delete_input=True)
    
    rs.EnableRedraw(True)
    
    
    pt_start = Rhino.Geometry.Point3d(0,0,0)
    obj = coordinates.Object
    
    
    def GetPointDynamicDrawFunc( sender, args ):
        rhobj = args.Source.Tag
        args.Display.DrawObject(rhobj)
    gp = Rhino.Input.Custom.GetPoint()
    gp.DrawLineFromPoint(pt_start, True)
    gp.SetCommandPrompt("Start of line")
    gp.DynamicDraw += GetPointDynamicDrawFunc
    gp.Tag = obj
    gp.Get()
    
    
    
    
    
    
WorldPlanes()