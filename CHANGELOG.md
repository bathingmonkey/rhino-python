# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.5] - 2020-09-15  
### Added
- TogglePanels

## [1.0.4] - 2020-06-12  
### Added
- MoveSketchLayer

## [1.0.3] - 2019-06-06
### Changed
- ChangeObjectColor_cmd.py accepts pre-selected objects

## [1.0.2] - 2019-06-01
### Fixed
- SlotCenterDist
- SlotCorner

## [1.0.1] - 2019-05-24
### Added  
- SlotCenter_cmd.py

### Changed
- Now SlotCenter_cmd.py constrains the direction to the normal to surface.

## [1.0.0] - 2019-04-06  
### Added
- SlotCenterDist
- SlotCorner
- ChangeObjectColor
- GridSwitch
- HideLockedObjects
- ShowLockedObjects
- toggle_gumball_drag_mode