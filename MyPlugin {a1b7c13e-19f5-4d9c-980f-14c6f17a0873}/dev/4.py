#import rhinoscriptsyntax as rs
#
#obj = rs.GetObject("Select a surface", rs.filter.surface)
#
#if rs.IsSurfacePlanar(obj):
#
#    print "The surface is planar."
#
#else:
#
#    print "The surface is not planar."



#import rhinoscriptsyntax as rs
#
#surface = rs.GetObject("Select surface to trim", rs.filter.surface)
#
#if surface and rs.IsPlaneSurface(surface):
#
#    print "got a plane surface"
#
#else:
#
#    print "not a plane surface"
#
#


#import rhinoscriptsyntax as rs
#
#obj = rs.GetObject("Select a Brep")
#
#if rs.IsBrep(obj):
#
#    print "The object is a Brep."
#
#else:
#
#    print "The object is not a Brep."




#import rhinoscriptsyntax as rs
#
#obj = rs.GetObject("Select a polysurface")
#
#if rs.IsPolysurface(obj):
#
#    print "The object is a polysurface."
#
#else:
#
#    print "The object is not a polysurface."
#    


#import rhinoscriptsyntax as rs
#
#objectId = rs.GetObject("Select a surface")
#
#if rs.IsSurface(objectId):
#
#    print "The object is a surface."
#
#else:
#
#    print "The object is not a surface."




#import rhinoscriptsyntax as rs
#
#surface_id = rs.GetObject("Select surface to draw curve on", rs.filter.surface)
#
#if surface_id:
#
#    domainU = rs.SurfaceDomain( surface_id, 0)
#
#    u0 = domainU[0]/2
#
#    u1 = domainU[1]/2
#
#    domainV = rs.SurfaceDomain( surface_id, 1)
#
#    v0 = domainV[0]/2
#
#    v1 = domainV[1]/2
#
#    rs.AddInterpCrvOnSrfUV( surface_id, [[u0,v0],[u1,v1]])


#import rhinoscriptsyntax as rs
#
#curve0 = rs.AddLine((0,0,0), (0,9,0))
#
#curve1 = rs.AddLine((1,10,0), (10,10,0))
#
#curves = curve0, curve1
#
#domain_crv0 = rs.CurveDomain(curve0)
#
#domain_crv1 = rs.CurveDomain(curve1)
#
#params = domain_crv0[1], domain_crv1[0]
#
#revs = False, True
#
#cont = 2,2
#
#rs.AddBlendCurve( curves, params, revs, cont )

#import rhinoscriptsyntax as rs
#
#surface0 = rs.GetObject("First surface", rs.filter.surface)
#
#surface1 = rs.GetObject("Second surface", rs.filter.surface)
#
#rs.FilletSurfaces(surface0, surface1, 3.0)




import rhinoscriptsyntax as rs

obj_id = rs.GetObject("Select object")
if obj_id <> None:
  print "The object's unique id is {0}" .format(obj_id)
