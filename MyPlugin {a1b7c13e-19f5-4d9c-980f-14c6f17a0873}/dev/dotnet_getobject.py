import Rhino

def OrientOnSrf():
    go = Rhino.Input.Custom.GetObject()
    go.SetCommandPrompt("Select objects")
    go.SubObjectSelect = False
    go.GroupSelect = False
    go.GeometryFilter = Rhino.DocObjects.ObjectType.Surface
    go.GetMultiple(2, 2)
    if go.CommandResult()!=Rhino.Commands.Result.Success:
        return go.CommandResult()
    
    
if __name__=="__main__":
    OrientOnSrf()