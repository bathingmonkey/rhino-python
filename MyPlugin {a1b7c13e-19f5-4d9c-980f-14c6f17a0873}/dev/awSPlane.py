'''
'''
import rhinoscriptsyntax as rs
import Rhino
import scriptcontext
import System.Guid


def UprightPlane(length=100, height=100):
    '''
    make a plane upright to cp
    '''
    
    line = MakeLine()
    line.AddLine()
    if line == Rhino.Commands.Result.Failure:
        return Rhino.Commands.Result.Failure
    
    p0 = line.pt_start
    p1 = line.pt_end
    vector = rs.VectorCreate(p1, p0)
    normal_cp = [0, 0, 1]
    plane = rs.PlaneFromFrame(p0, vector, normal_cp )
    rs.AddPlaneSurface(plane, length, height)
    rs.DeleteObject(line.id)
    return plane


class MakeLine(object):
    '''
    '''
    
    def __init__(self):
        '''
        '''
        
        self.id = System.Guid.Empty
        self.pt_start = (0, 0, 0)
        self.pt_end = (0, 0, 0)
        
    def AddLine(self):
        '''
        '''
        
        gp = Rhino.Input.Custom.GetPoint()
        gp.SetCommandPrompt("Start of line")
        gp.Get()
        if gp.CommandResult()!=Rhino.Commands.Result.Success:
            return gp.CommandResult()
        self.pt_start = gp.Point()
        #
        gp.SetCommandPrompt("End of line")
        gp.SetBasePoint(self.pt_start, False)
        gp.DrawLineFromPoint(self.pt_start, True)
        gp.Get()
        if gp.CommandResult() != Rhino.Commands.Result.Success:
            return gp.CommandResult()
        self.pt_end = gp.Point()
        #
        v = self.pt_end - self.pt_start
        if v.IsTiny(Rhino.RhinoMath.ZeroTolerance):
            return Rhino.Commands.Result.Nothing
        #
        self.id = scriptcontext.doc.Objects.AddLine(self.pt_start, self.pt_end)
        if self.id!=System.Guid.Empty:
            scriptcontext.doc.Views.Redraw()
            return Rhino.Commands.Result.Success
        return Rhino.Commands.Result.Failure
    
print UprightPlane()