# 2020/6/25

import rhinoscriptsyntax as rs
import scriptcontext as sc
import Rhino
import Rhino.ApplicationSettings as settings

__commandname__ = "GridSize"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filename minus "_cmd.py"


def RunCommand(is_interactive):
    """docstring"""
    print(__commandname__)

    # you can optionally return a value from this function
    # to signify command result. Return values that make
    # sense are
    # 0 == success
    # 1 == cancel
    # If this function does not return a value, success is assumed
    return 0


# This allows you to test the script from the editor, debug etc.
# RunCommand(True)
if(__name__ == "__main__"):
    RunCommand(True)
