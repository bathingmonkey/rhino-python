# 2018/04/07
import rhinoscript.userinterface
import rhinoscript.geometry
import rhinoscriptsyntax as rs
import Rhino


__commandname__ = "ChangeObjectColor"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def GetColorAlpha(color=[0,0,0,255]):
    #returns a tuple (r,g,b,a)
    color = rs.coercecolor(color)
    if color is None: color = System.Drawing.Color.Black
    color=Rhino.Display.Color4f(color)
    rc, c = Rhino.UI.Dialogs.ShowColorDialog(color,True)
    if rc: return (int(c.R*255),int(c.G*255),int(c.B*255),int(c.A*255))

def RunCommand( is_interactive ):
    obj_ids=rs.GetObjects("Select objects to change color", preselect=True)
    if obj_ids:
        obj_color=GetColorAlpha(rs.ObjectColor(obj_ids[0]))
        if obj_color:
            rs.ObjectColor(obj_ids,obj_color)
    rs.Redraw()
    # you can optionally return a value from this function
    # to signify command result. Return values that make
    # sense are
    #   0 == success
    #   1 == cancel
    # If this function does not return a value, success is assumed
    return 0
if( __name__ == "__main__" ):
    RunCommand(True)
