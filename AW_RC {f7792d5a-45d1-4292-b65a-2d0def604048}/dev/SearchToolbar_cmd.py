import Rhino
import scriptcontext as sc
import os
import fnmatch
import rhinoscriptsyntax as rs
import Eto.Drawing as drawing
import Eto.Forms as forms

__commandname__ = "SelectToolbar"



"""
ChangeLog:
    Try using Table or Gridview


"""



class SelectToolbarDialog(forms.Dialog[bool]):
    
    # Initializer
    def __init__(self):
        # Eto initials
        self.Title = "Select Toolbar"
        self.Padding = drawing.Padding(5)
        self.Resizable = True
        self.Maximizable = False
        self.Minimizable = False
        self.ShowInTaskbar = False
        self.MinimumSize = drawing.Size(250, 250)
        
        # Create a label control
        label = forms.Label()
        label.Text = 'Select a toolbar'
        
        # fields
        self.ToolbarList = self.InitializeToolbarList()
        self.SearchedToolbarList = self.ToolbarList[::]
#        self.SearchedToolbarList = self.InitializeToolbarList()
        
        # initialize layout
        layout = forms.DynamicLayout()
        layout.Padding = drawing.Padding(5)
        layout.Spacing = drawing.Size(5, 5)
        
        # add label
        layout.AddRow(label)
        
        # add search
        layout.BeginVertical()
        layout.AddRow(*self.CreateSearchBar())
        layout.EndVertical()
        
        # add listBox
        layout.BeginVertical()
        layout.AddRow(self.CreateToolbarListBox())
        layout.EndVertical()
        
        # add buttons
        layout.AddSeparateRow(self.CreateButtons())
        
        # set content
        self.Content = layout
        
        
    def InitializeToolbarList(self):
        toolbar_set = set()
        for tool in rs.ToolbarCollectionNames():
            for toolbar in rs.ToolbarNames(tool):
                toolbar_set.add(toolbar)
        return sorted(list(toolbar_set))
        
    def CreateSearchBar(self):
        self.lbl_Search = forms.Label()
        self.lbl_Search.Text = "Search:"
        self.lbl_Search.VerticalAlignment = forms.VerticalAlignment.Center
        
        self.tB_Search = forms.TextBox()
        self.tB_Search.TextChanged += self.tB_Search_TextChanged
        
        return [self.lbl_Search, self.tB_Search]
        
    def CreateToolbarListBox(self):
        self.lb = forms.ListBox()
        self.lb.Size = drawing.Size(200, 500)
        self.lb.DataStore = self.SearchedToolbarList
        self.lb.MouseDoubleClick += self.lb_DoubleClick
        
        return self.lb
        
    def CreateButtons(self):
        self.OpenButton = forms.Button()
        self.OpenButton.Text = "Open"
        self.OpenButton.Click += self.OnOpenButtonClicked
        
        self.CancelButton = forms.Button()
        self.CancelButton.Text = "Cancel"
        self.CancelButton.Click += self.OnCancelButtonClicked
        
        # Create button layout
        layout = forms.DynamicLayout()
#        layout.Padding = drawing.Padding(10)
        layout.Spacing = drawing.Size(5, 5)
#        layout.MinimumSize = drawing.Size(100, 100)
#        layout.Height = 40
#        self.ClientSize = drawing.Size(300, 600) 
#        for i in dir(layout):
#            print i
        layout.AddRow(None, self.OpenButton, self.CancelButton, None)

        return layout
        
        #return [self.OpenButton, self.CancelButton]
        
    def Search(self, text):
        """
        Searches self.ToolbarList with a given string
        Supports wildCards
        """
        if text == "":
            self.lb.DataStore = self.ToolbarList
        else:
            self.SearchedToolbarList = fnmatch.filter(self.ToolbarList, "*" + text + "*")
            self.lb.DataStore = self.SearchedToolbarList
            
    def OpenToolbar(self):
        if self.lb.SelectedValue:
            self.Visible = False
            print "Open toolbar: " + self.lb.SelectedValue
            rs.Command("-_ShowToolbar \"{}\"".format(self.lb.SelectedValue))
            self.Visible = True
        
    def tB_Search_TextChanged(self, sender, e):
        """
        event handler handling text input in ther search bar
        """
        self.Search(self.tB_Search.Text)
        
    def OnOpenButtonClicked(self, sender, e):
        self.OpenToolbar()
        self.Close(True)
        
    def OnCancelButtonClicked(self, sender, e):
        self.Close(False)
        
    def lb_DoubleClick(self, sender, e):
        self.OpenToolbar()
        self.Close(True)

def RunCommand(is_interactive):
    dialog = SelectToolbarDialog()
    rc = Rhino.UI.EtoExtensions.ShowSemiModal(dialog, Rhino.RhinoDoc.ActiveDoc,\
        Rhino.UI.RhinoEtoApp.MainWindow)

if __name__ == "__main__":
    RunCommand(True)
    