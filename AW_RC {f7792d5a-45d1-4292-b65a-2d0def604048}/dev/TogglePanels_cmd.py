import rhinoscriptsyntax as rs
import scriptcontext as sc
import Rhino.UI as ru
import os, string
import ConfigParser
from distutils.util import strtobool

__commandname__ = "TogglePanels"


def check_opened_panels_count():
    return len(ru.Panels.GetOpenPanelIds())


def hide_panels(panel_ids):
    panels_info = []
    for id in panel_ids:
        panels_info.append((id, ru.Panels.IsPanelVisible(id, True)))
    l = reversed(panel_ids)
    for id in l:
        ru.Panels.ClosePanel(id)
    return panels_info


def show_panels(panels_info):
    for id, makeSelectedPanel in panels_info:
        ru.Panels.OpenPanel(id, makeSelectedPanel)
    return


# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def RunCommand( is_interactive ):
    print __commandname__


    FILENAME = os.path.join(os.path.dirname(__file__), "togglepanels.cfg")
    panels_info = []
    
    
    # confirm if there is the cfg file
    try:
        cp = ConfigParser.ConfigParser()
        cp.read(FILENAME)
        if cp.sections():
            for index in cp.sections():
                id = rs.coerceguid(cp.get(index, "panel_id"))
                panel_activated = bool(strtobool(cp.get(index, "panel_activated")))
                panels_info.append((id, panel_activated))
    except Exception as e:
        pass
        #print "file is broken
        #print e
        

    
    # close opened panels if there is panels opened
    if check_opened_panels_count():
        # closeing
        panel_ids = ru.Panels.GetOpenPanelIds()
        panels_info = hide_panels(panel_ids)
        if len(panels_info) == len(panel_ids):
            print "closed {} panels".format(len(panels_info))
        # writing cfg
        cp = ConfigParser.ConfigParser()
        for index, (id, panel_activated) in enumerate(panels_info):
            cp.add_section(str(index))
            cp.set(str(index), "panel_id", str(id))
            cp.set(str(index), "panel_activated", str(panel_activated))
        with open(FILENAME, 'w') as f:
            cp.write(f)
    else:
        # opening
        if panels_info:
            show_panels(panels_info) 
        else: print "No last opened panel"  


    # you can optionally return a value from this function
    # to signify command result. Return values that make
    # sense are
    #   0 == success
    #   1 == cancel
    # If this function does not return a value, success is assumed
    return 0


if( __name__ == "__main__" ):
    RunCommand(True)